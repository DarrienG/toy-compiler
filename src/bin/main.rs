#![deny(clippy::pedantic, warnings, clippy::all, clippy::nursery)]
use dg_compiler::r0::{add, negate, number, Program};

fn main() {
    let simple_program = Program::new(add(number(5), negate(number(5))));

    println!(
        "AST: {:?}, output: {}",
        simple_program,
        simple_program.run()
    );
}
