use std::{
    collections::HashMap,
    fmt::{self, Display, Formatter},
    string::ToString,
};

struct RegisterSet<'a>(&'a str);

impl<'a> Display for RegisterSet<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

type Reg<'a> = RegisterSet<'a>;

impl<'a> RegisterSet<'a> {
    pub const RSP: Reg<'a> = RegisterSet("%rsp");
    pub const RBP: Reg<'a> = RegisterSet("%rbp");
    pub const RAX: Reg<'a> = RegisterSet("%rax");
    pub const RBX: Reg<'a> = RegisterSet("%rbx");
    pub const RCX: Reg<'a> = RegisterSet("%rcx");
    pub const RDX: Reg<'a> = RegisterSet("%rdx");
    pub const RSI: Reg<'a> = RegisterSet("%rsi");
    pub const RDI: Reg<'a> = RegisterSet("%rdi");
    pub const R8: Reg<'a> = RegisterSet("%r8");
    pub const R9: Reg<'a> = RegisterSet("%r9");
    pub const R10: Reg<'a> = RegisterSet("%r10");
    pub const R11: Reg<'a> = RegisterSet("%r11");
    pub const R12: Reg<'a> = RegisterSet("%r12");
    pub const R13: Reg<'a> = RegisterSet("%r13");
    pub const R14: Reg<'a> = RegisterSet("%r14");
    pub const R15: Reg<'a> = RegisterSet("%r15");
}

enum Arg<'a> {
    Constant(i64),
    Register(RegisterSet<'a>),
    Deref(RegisterSet<'a>, i64),
    Ref(&'a str),
}

impl<'a> Display for Arg<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Arg::Constant(v) => v.to_string(),
                Arg::Register(v) => v.to_string(),
                Arg::Deref(rs, v) => format!("({}), {}", rs, v),
                Arg::Ref(v) => format!("[{}]", v),
            },
        )
    }
}

enum Instr<'a> {
    Addq(Arg<'a>, Arg<'a>),
    Subq(Arg<'a>, Arg<'a>),
    Movq(Arg<'a>, Arg<'a>),
    Pushq(Arg<'a>),
    Popq(Arg<'a>),
}

impl<'a> Display for Instr<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Instr::Addq(arg1, arg2) => format!("addq {} {}", arg1, arg2),
                Instr::Subq(arg1, arg2) => format!("subq {} {}", arg1, arg2),
                Instr::Movq(arg1, arg2) => format!("movq {} {}", arg1, arg2),
                Instr::Pushq(arg) => format!("pushq {}", arg),
                Instr::Popq(arg) => format!("popq {}", arg),
            }
        )
    }
}

struct BlkInstruction<'a> {
    instructions: Vec<Instr<'a>>,
}

impl<'a> Display for BlkInstruction<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            self.instructions
                .iter()
                .map(ToString::to_string)
                .fold(String::new(), |mut a, b| {
                    a.reserve(b.len() + 1);
                    a.push_str(&b);
                    a.push('\n');
                    a
                })
        )
    }
}

struct X0Program<'a> {
    instruction_labels: HashMap<&'a str, BlkInstruction<'a>>,
    start_label: &'a str,
}
