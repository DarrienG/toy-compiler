use std::collections::HashMap;
use std::io::{self, Write};

#[derive(Debug, Clone)]
pub struct Program {
    expression: Box<Expression>,
}

impl Program {
    #[must_use]
    pub fn run(&self) -> i64 {
        self.expression.eval(HashMap::new())
    }

    #[must_use]
    pub fn new(expression: Box<Expression>) -> Self {
        Program { expression }
    }

    #[must_use]
    pub fn optimize(&self) -> Self {
        Program {
            expression: self.expression.optimize(HashMap::new()),
        }
    }

    #[must_use]
    pub fn size(&self) -> u64 {
        self.expression.size()
    }
}

#[derive(Debug, Clone)]
pub enum Expression {
    Number(i64),
    Read(bool),
    Negate(Box<Expression>),
    Add(Box<Expression>, Box<Expression>),
    Let(String, Box<Expression>, Box<Expression>),
    Var(String),
}

impl Expression {
    #[must_use]
    fn eval(&self, mut env: HashMap<String, i64>) -> i64 {
        match self {
            Expression::Number(v) => *v,
            Expression::Read(execute) => {
                if *execute {
                    Self::read_input()
                } else {
                    10
                }
            }
            Expression::Negate(v) => -v.eval(env),
            Expression::Add(v1, v2) => v1.eval(env.clone()) + v2.eval(env),
            Expression::Let(var_name, var_exp, exp) => {
                let old_env = env.clone();
                env.insert(var_name.to_owned(), var_exp.eval(old_env));
                exp.eval(env)
            }
            Expression::Var(var_name) => match env.get(var_name) {
                Some(v) => *v,
                None => panic!(
                    "Attempted to access variable `{}` that did not exist",
                    var_name
                ),
            },
        }
    }

    #[must_use]
    pub fn optimize(&self, env: HashMap<String, i64>) -> Box<Expression> {
        match self {
            Expression::Number(v) => number(*v),
            Expression::Read(v) => read(*v),
            Expression::Negate(exp) => Self::optimize_negate(env, exp),
            Expression::Add(exp1, exp2) => Self::optimize_add(env, exp1, exp2),
            Expression::Let(var_name, var_exp, exp) => {
                Self::optimize_let(env, var_name, var_exp, exp)
            }
            Expression::Var(var_name) => Self::optimize_var(&env, var_name),
        }
    }

    #[must_use]
    pub fn optimize_add(
        env: HashMap<String, i64>,
        exp1: &Expression,
        exp2: &Expression,
    ) -> Box<Expression> {
        let opt_exp1 = exp1.optimize(env.clone());
        let opt_exp2 = exp2.optimize(env);

        match *opt_exp1 {
            Expression::Number(v1) => match *opt_exp2 {
                Expression::Number(v2) => number(v1 + v2),
                _ => add(opt_exp1, opt_exp2),
            },
            _ => add(opt_exp1, opt_exp2),
        }
    }

    #[must_use]
    pub fn size(&self) -> u64 {
        match self {
            Expression::Number(_) | Expression::Read(_) | Expression::Var(_) => 1,
            Expression::Negate(exp) => 1 + exp.size(),
            Expression::Add(exp1, exp2) => 1 + exp1.size() + exp2.size(),
            Expression::Let(_, var_exp, exp) => 1 + var_exp.size() + exp.size(),
        }
    }

    fn optimize_let(
        mut env: HashMap<String, i64>,
        var_name: &str,
        var_exp: &Expression,
        exp: &Expression,
    ) -> Box<Expression> {
        let optimized_var_exp = var_exp.optimize(env.clone());
        match *optimized_var_exp {
            Expression::Number(v) => {
                env.insert(var_name.to_owned(), v);
                let optimized_run_exp = exp.optimize(env);
                match *optimized_run_exp {
                    Expression::Number(exp_v) => number(exp_v),
                    _ => let_set(var_name, optimized_var_exp, optimized_run_exp),
                }
            }
            _ => let_set(var_name, optimized_var_exp, exp.optimize(env)),
        }
    }

    fn optimize_var(env: &HashMap<String, i64>, var_name: &str) -> Box<Expression> {
        match env.get(var_name) {
            Some(v) => number(*v),
            None => var(var_name),
        }
    }

    fn optimize_negate(env: HashMap<String, i64>, expr: &Expression) -> Box<Expression> {
        let optimized = expr.optimize(env);
        match *optimized {
            Expression::Number(v) => number(-v),
            _ => negate(optimized),
        }
    }

    fn read_input() -> i64 {
        let mut input = String::new();

        print!("Enter read input: ");
        io::stdout().flush().unwrap();
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");

        input
            .trim()
            .parse::<i64>()
            .expect("bad input, turn this into a result later")
    }
}

#[must_use]
pub fn number(value: i64) -> Box<Expression> {
    Box::new(Expression::Number(value))
}

#[must_use]
pub fn read(execute: bool) -> Box<Expression> {
    Box::new(Expression::Read(execute))
}

#[must_use]
pub fn add(exp1: Box<Expression>, exp2: Box<Expression>) -> Box<Expression> {
    Box::new(Expression::Add(exp1, exp2))
}

#[must_use]
pub fn negate(exp: Box<Expression>) -> Box<Expression> {
    Box::new(Expression::Negate(exp))
}

#[must_use]
pub fn let_set(var_name: &str, var_exp: Box<Expression>, exp: Box<Expression>) -> Box<Expression> {
    Box::new(Expression::Let(var_name.to_owned(), var_exp, exp))
}

#[must_use]
pub fn var(var_name: &str) -> Box<Expression> {
    Box::new(Expression::Var(var_name.to_owned()))
}
