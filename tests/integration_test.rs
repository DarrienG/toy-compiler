use dg_compiler::r0::{add, let_set, negate, number, read, var, Expression, Program};
use rand::{seq::SliceRandom, Rng};

#[test]
fn it_adds_two() {
    assert_eq!(Program::new(add(number(2), number(2))).run(), 4);
}

#[test]
fn it_negates_and_adds() {
    assert_eq!(Program::new(add(number(3), negate(number(10)))).run(), -7);
}

#[test]
fn it_can_deeply_nest() {
    assert_eq!(
        Program::new(add(
            add(add(negate(number(5)), negate(number(5))), number(5)),
            number(5)
        ))
        .run(),
        0
    );
}

#[test]
fn it_evals_read_properly() {
    assert_eq!(Program::new(add(read(false), number(5))).run(), 15);
}

#[test]
fn it_calculates_power_of_two_s() {
    assert_eq!(power_of_two(2).run(), 4);
}

#[test]
fn it_calculates_power_of_two_l() {
    assert_eq!(power_of_two(10).run(), 1024);
}

#[test]
fn it_runs_random_programs() {
    let random_program = make_random_program();
    println!("generated: {:?}", random_program);
    let _ = random_program.run();
}

#[test]
fn it_optimizes_simple_programs() {
    let unoptimized = Program::new(add(number(1), number(1)));
    let optimized = unoptimized.optimize();

    assert_eq!(unoptimized.run(), optimized.run());
    assert_eq!(unoptimized.size(), 3);
    assert_eq!(optimized.size(), 1);
}

#[test]
fn it_optimizes_complicated_programs() {
    let unoptimized = Program::new(add(
        number(1),
        add(read(false), add(negate(number(1)), number(2))),
    ));
    let optimized = unoptimized.optimize();

    assert_eq!(unoptimized.run(), optimized.run());
    assert_eq!(unoptimized.size(), 8);
    assert_eq!(optimized.size(), 5);
}

#[test]
fn it_outputs_same_optimized_random() {
    for _ in 0..100 {
        let random_program = make_random_program();
        assert_eq!(random_program.run(), random_program.optimize().run());
    }
}

#[test]
fn it_stores_variables() {
    assert_eq!(
        Program::new(let_set("x", add(number(5), number(5)), var("x"))).run(),
        10
    );
}

#[test]
fn it_optimizes_let() {
    let program = Program::new(let_set("x", number(3), add(var("x"), number(2))));
    let optimized = program.optimize();
    assert_eq!(program.run(), optimized.run());
    assert_eq!(optimized.size(), 1);
}

fn power_of_two(power: u8) -> Program {
    if power == 0 {
        Program::new(number(1))
    } else {
        Program::new(calculate_power_of_two(2_u32.pow(power as u32) / 2))
    }
}

fn calculate_power_of_two(depth: u32) -> Box<Expression> {
    if depth == 1 {
        number(2)
    } else {
        add(number(2), calculate_power_of_two(depth - 1))
    }
}

fn make_random_program() -> Program {
    Program::new(random_expression_generator(
        rand::thread_rng().gen_range(1..15),
        Vec::new(),
    ))
}

enum HasChildren {
    Add,
    Negate,
    Let,
}

enum NoChildren {
    Read,
    Number,
    Var,
}

impl HasChildren {
    fn get_random() -> HasChildren {
        let mut rng = rand::thread_rng();
        let range = rng.gen_range(0..=2);

        match range {
            0 => HasChildren::Add,
            1 => HasChildren::Negate,
            2 => HasChildren::Let,
            _ => panic!("Unhandled branch"),
        }
    }
}

impl NoChildren {
    fn get_random() -> NoChildren {
        let mut rng = rand::thread_rng();
        let range = rng.gen_range(0..=2);

        match range {
            0 => NoChildren::Read,
            1 => NoChildren::Number,
            2 => NoChildren::Var,
            _ => panic!("Unhandled branch"),
        }
    }
}

fn random_expression_generator(depth: u8, variable_list: Vec<String>) -> Box<Expression> {
    if depth <= 1 {
        match NoChildren::get_random() {
            NoChildren::Read => read(false),
            NoChildren::Number => number(rand::thread_rng().gen_range(0..500)),
            NoChildren::Var => {
                if variable_list.is_empty() {
                    number(rand::thread_rng().gen_range(0..500))
                } else {
                    var(&variable_list
                        .choose_multiple(&mut rand::thread_rng(), 1)
                        .collect::<Vec<&String>>()
                        .first()
                        .unwrap())
                }
            }
        }
    } else {
        match HasChildren::get_random() {
            HasChildren::Add => add(
                random_expression_generator(depth - 1, variable_list.clone()),
                random_expression_generator(depth - 1, variable_list),
            ),
            HasChildren::Negate => negate(random_expression_generator(depth - 1, variable_list)),
            HasChildren::Let => let_set(
                &rand::thread_rng().gen_range(0..50000).to_string(),
                random_expression_generator(depth - 1, variable_list.clone()),
                random_expression_generator(depth - 1, variable_list),
            ),
        }
    }
}
